<?php 
include 'connection.php';
include 'function.php';

error_reporting(E_ALL ^ E_NOTICE);
session_start();

if ($_SESSION['name'] == "" && $_SESSION['level'] == "") {
  displayAlert("alert","You need to login first!");
  header("Location: login.php");
}
?>

<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Bootstrap CSS -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <!-- font style -->
  <link rel="preconnect" href="https://fonts.gstatic.com">  
  <!-- <link href="https://fonts.googleapis.com/css2?family=DM+Sans&display=swap" rel="stylesheet"> -->
  <!--   <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200&display=swap" rel="stylesheet">   -->
  <link href="https://fonts.googleapis.com/css2?family=Playfair+Display&family=Work+Sans&display=swap" rel="stylesheet">  
  <script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>

  <title>KainTenunKu-Customer</title>

</head>
<body>
  <!-- NAVBAR --> 
  <header>        
    <nav class="navbar navbar-expand-lg navbar-light bg-white fixed-top">
      <div class="container-fluid">
        <a class="navbar-brand" href="#">
          <img class="logo" src="img/logokecil.png" width="110" height="50">
        </a>        
        <div class="collapse navbar-collapse col-md-6" id="navbarNav">          
          <ul class="navbar-nav navbar-right">
            <li class="nav-item">
              <a class="nav-link " aria-current="page" href="customerIndex.php">Home</a>
            </li>

            <li class="nav-item">
              <a class="nav-link " aria-current="page" href="productList.php">Product List</a>
            </li>

            <li class="nav-item">
              
            </li>

            <li class="nav-item">
              <a class="nav-link " aria-current="page" href="#">Payment</a>
            </li>

            <li class="nav-item">
              <a class="nav-link " href="logout.php">Logout</a>
            </li>

            <li class="nav-item">

            </li>
          </ul>        
        </div>
        <div class="navbar navbar-nav col-2">                        
          <div class="collapse navbar-collapse">
            <?php echo $_SESSION['name'].''."<b><p class='card-text'><i class='fas fa-user-alt' style='margin-left:10px;font-size:20px'></i></p></b>" ?>

            <i class="fa fa-shopping-cart" aria-hidden="true" 
              style="
              font-size: 20px;
              margin-left: 20px;
              margin-top: 16px;">
              <a class="nav-link " href="Cart.php"></a>            
            </i> 

            <i class="fa fa-comment mt-3 ml-5" aria-hidden="true"               
              style="
              font-size: 23px;
              margin-left: 20px;">
              <a class="nav-link " href="Chat.php"></a>            
            </i> 

          </div>          
        </div> 
      </div> 

      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>                
    </nav>    
  </header>  

  <section id="top">
    <div class="row no-gutters bg-light mt-5 pt-4">
      <div class="container-fluid bg-light">
        <div class="row">
          <div class="col-4 m-4">
            <ul class="list-group list-group-flush">
              <li class="list-group-item"><h3><b>PRODUCT CATEGORY</b></h3></li>
              <li class="list-group-item"><a style="color: black;" class="nav-link active" aria-current="page" href="#Anahida">Anahida</a></li>
              <li class="list-group-item"><a style="color: black;" class="nav-link active" aria-current="page" href="#Kaleku">Kaleku</a></li>
              <li class="list-group-item"><a style="color: black;" class="nav-link active" aria-current="page" href="#Mamuli">Mamuli</a></li>
              <li class="list-group-item"><a style="color: black;" class="nav-link active" aria-current="page" href="#Haikara">Haikara</a></li>
              <li class="list-group-item"><a style="color: black;" class="nav-link active" aria-current="page" href="#Tabelo">Tabelo</a></li>
              <li class="list-group-item"><a style="color: black;" class="nav-link active" aria-current="page" href="#Hinggi">Hinggi Kombu</a></li>
            </ul>
          </div>
          <div class="col-7 m-3">
           <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-inner">
              <div class="carousel-item active">
                <img src="img/2.JPG" class="d-block w-100" alt="...">
                <div class="carousel-caption d-none d-md-block">                  
                </div>
              </div>
              <div class="carousel-item">
                <img src="img/3.JPG" class="d-block w-100" alt="...">
                <div class="carousel-caption d-none d-md-block">                  
                </div>
              </div>
              <div class="carousel-item">
                <img src="img/1.jpg" class="d-block w-100" alt="...">
                <div class="carousel-caption d-none d-md-block">                  
                </div>
              </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Next</span>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Display Product -->      
  <div class="container">    
    <div class="bg-warning mt-5 p-2">
      <h3 class="text-center font-weight-bold m-4" id="Anahida"><b>AnaHida</b></h3>
    </div>

    <div class="row justify-content-center bg-light mt-5">      
      <?php displayProduct("Anahida"); ?>    
    </div>
    
    <div class="bg-warning m-2 mt-5 p-1 ">
      <h3 class="text-center font-weight-bold m-5" id="Kaleku"><b>Kaleku</b></h3>
    </div>

    <div class="row justify-content-center bg-light mt-2">      
      <?php displayProduct("Kaleku"); ?>          
    </div>


    <div class="bg-warning m-2 mt-5 p-1 ">
      <h3 class="text-center font-weight-bold m-4" id="Mamuli"><b>Mamuli</b></h3>
    </div>

    <div class="row justify-content-center bg-warning mt-5">
      <?php displayProduct("Mamuli"); ?>    
    </div>

    <div class="bg-warning m-2 mt-5 p-1 ">
      <h3 class="text-center font-weight-bold m-4" id="Haikara"><b>Haikara</b></h3>
    </div>

    <div class="row justify-content-center bg-light mt-5">      
      <?php displayProduct("Haikara"); ?>          
    </div>

    <div class="bg-warning m-2 mt-5 p-1 ">
      <h3 class="text-center font-weight-bold m-4" id="Tabelo"><b>Tabelo</b></h3>
    </div>

    <div class="row justify-content-center bg-warning mt-5">      
      <?php displayProduct("Tabelo"); ?>    
    </div>

    <div class="bg-warning m-2 mt-5 p-1 ">
      <h3 class="text-center font-weight-bold m-4" id="Hinggi Kombu"><b>Hinggi Kombu</b></h3>
    </div>

    <div class="row justify-content-center bg-light mt-5 mb-5">      
      <?php displayProduct("Hinggi Kombu"); ?>          
    </div>

    <div class="row justify-content-end">
      <div class="col-2">
        <a href="#top" class="btn btn-primary">Back to Top</a>
      </div>      
    </div>
  </div>

</section>
</body>

<footer class="bg-light text-center text-lg-start">
  <!-- Copyright -->
  <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
    © 2021 Copyright:
    <a class="text-dark" href="index.php">KAINTENUNKU.com</a>
  </div>
  <!-- Copyright -->
</footer>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>

<script src="function.js"></script>

<script src="https://kit.fontawesome.com/yourcode.js" crossorigin="anonymous"></script>

<!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js" integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js" integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG" crossorigin="anonymous"></script>
  -->
  
  </html>