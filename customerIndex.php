<?php 
include 'connection.php';
include 'function.php';

error_reporting(E_ALL ^ E_NOTICE);
session_start();

if ($_SESSION['name'] == "" && $_SESSION['level'] == "") {
  displayAlert("alert","You need to login first!");
  header("Location: login.php");
}
?>

<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Bootstrap CSS -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <!-- font style -->
  <link rel="preconnect" href="https://fonts.gstatic.com">  
  <!-- <link href="https://fonts.googleapis.com/css2?family=DM+Sans&display=swap" rel="stylesheet"> -->
  <!--   <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200&display=swap" rel="stylesheet">   -->
  <link href="https://fonts.googleapis.com/css2?family=Playfair+Display&family=Work+Sans&display=swap" rel="stylesheet">  
  <script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>

  <title>KainTenunKu-Home</title>

</head>
<body>
  <!-- INI NAVBAR -->
  <header>        
    <nav class="navbar navbar-expand-lg navbar-light bg-white fixed-top">
      <div class="container-fluid">
        <a class="navbar-brand" href="#">
          <img class="logo" src="img/logokecil.png" width="110" height="50">
        </a>        
        <div class="collapse navbar-collapse col-md-6" id="navbarNav">          
          <ul class="navbar-nav navbar-right">
            <li class="nav-item">
              <a class="nav-link " aria-current="page" href="customerIndex.php">Home</a>
            </li>

            <li class="nav-item">
              <a class="nav-link " aria-current="page" href="productList.php">Product List</a>
            </li>

            <li class="nav-item">

            </li>

            <li class="nav-item">
              <a class="nav-link " aria-current="page" href="#">Payment</a>
            </li>

            <li class="nav-item">
              <a class="nav-link " href="logout.php">Logout</a>
            </li>

            <li class="nav-item">

            </li>
          </ul>        
        </div>
        <div class="navbar navbar-nav col-2">                        
          <div class="collapse navbar-collapse">
            <?php echo $_SESSION['name'].''."<b><p class='card-text'><i class='fas fa-user-alt' style='margin-left:10px;font-size:20px'></i></p></b>" ?>

            <i class="fa fa-shopping-cart" aria-hidden="true" 
            style="
            font-size: 20px;
            margin-left: 20px;
            margin-top: 16px;">
            <a class="nav-link " href="Cart.php"></a>            
          </i> 

          <i class="fa fa-comment mt-3 ml-5" aria-hidden="true"               
          style="
          font-size: 23px;
          margin-left: 20px;">
          <a class="nav-link " href="Chat.php"></a>            
        </i> 

      </div>          
    </div> 
  </div> 

  <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>                
</nav>

</header>


<!-- CAROUSEL -->
<section>
  <div class="col-md-12 mt-5">
    <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
      <div class="carousel-indicators">
        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
      </div>

      <div class="carousel-inner col-xs-3">
        <div class="carousel-item active">
          <img src="img/carousel3.jpg" class="d-block w-100">
          <div class="carousel-caption d-none d-md-block">
            <h1 class="one">Sumbanese Original <br> Products </h1>
            <a href="productList.php">
              <p>Original product from the handwork of Sumbanese women.</p></a>        
            </div>
          </div>
          <div class="carousel-item">
            <img src="img/carousel1.jpg" class="d-block w-100">
            <div class="carousel-caption d-none d-md-block">
             <h1 style="text-align: center;">Hinggi Kombu & Kaliuda</h1>
             <a href="productList.php">            
              <button type="button" class="btn btn-outline-light btn-lg">Available Now!</button>
              <button type="button" class="btn btn-primary btn-lg">GO SHOPPING</button>
            </a>          
          </div>
        </div>
        <div class="carousel-item">
          <img src="img/carousel2.jpg" class="d-block w-100">
          <div class="carousel-caption d-none d-md-block">
           <h1>100% Guaranted <br>Originality! </h1>
           <a href="https://mail.google.com/mail/u/0/#inbox?compose=CllgCJNqLHZLdDxvZhvgjXvhTfFttSDbddsCrQWWWdDlzjHBPxVMDQVdwjqpqmdBmDBHCFGZvhg">
            <p class="">Go check out our latest product!</p>
          </a>
        </div>
      </div>

    </div>
    <div>
      <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions"  data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previous</span>
      </button>
      <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions"  data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
      </button>
    </div>
  </div>
  <!-- Category -->    
</section>

<section>

  <div class="col-md-12" id="product">
    <div class="row mx-auto justify-content-center">
      <h3 class="text-center font-weight-bold m-5"><b>Product Description</b></h3>          
      <div class="card justify-content-center" style="width: 18rem;">
        <img src="img/kategori2.jpg" class="card-img-top" alt="...">
        <div class="card-body">
          <h5 class="card-title">AnaHida</h5>
          <p class="card-text">Anahida or Muti salak, a traditional antique bead with a beautiful orange color from Sumba.</p>
          <a href="#" class="btn btn-primary">Add to Cart</a>
        </div>
      </div>    

      <div class="card justify-content-center" style="width: 18rem;">
        <img src="img/kategori1.jpg" class="card-img-top" alt="...">
        <div class="card-body">
          <h5 class="card-title">Hinggi Kombu</h5>
          <p class="card-text">Hinggi Kombu, a woven cloth designed by men for daily use especially for traditional event.</p>
          <a href="#" class="btn btn-primary">Add to Cart</a>
        </div>
      </div>    

      <div class="card justify-content-center" style="width: 18rem;">
        <img src="img/kategori3.jpg" class="card-img-top" alt="...">
        <div class="card-body">
          <h5 class="card-title">Mamuli</h5>
          <p class="card-text">Mamuli, a precious metal jewelery piece from the Sumba tribe on the island of Sumba, Indonesia.</p>
          <a href="#" class="btn btn-primary">Add to Cart</a>
        </div>
      </div>    
    </div>

    <div class="row mx-auto justify-content-center">        
      <div class="card justify-content-center" style="width: 18rem;">
        <img src="img/haikara.jpg" class="card-img-top" alt="...">
        <div class="card-body">
          <h5 class="card-title">Haikara</h5>
          <p class="card-text">The headdress of a three-pronged rattan blade decorated with horsetail hair. Worn by pinning on kapouta.</p>
          <a href="#" class="btn btn-primary">Add to Cart</a>
        </div>
      </div>    

      <div class="card justify-content-center" style="width: 18rem;">
        <img src="img/kaleku.jpg" class="card-img-top" alt="...">
        <div class="card-body">
          <h5 class="card-title">Kaleku</h5>
          <p class="card-text">traditional woven pandanus or bark bag draped over the left shoulder. Its use is the same as that used by men.</p>
          <a href="#" class="btn btn-primary">Add to Cart</a>
        </div>
      </div>

      <div class="card justify-content-center" style="width: 18rem;">
        <img src="img/tabelo.jpg" class="card-img-top" alt="...">
        <div class="card-body">
          <h5 class="card-title">Tabelo</h5>
          <p class="card-text">The head jewelery has the shape of a round crescent or buffalo horn, made of gold or silver.</p>
          <a href="#" class="btn btn-primary">Add to Cart</a>
        </div>
      </div>     
    </div>
  </div>    
</section>

<!-- FACT OF THE DAY -->
<section id="fact">
 <div class="col-md-12 mt-10">
  <div class="row">            
    <div class="col-md-4">
      <h1 style="font-size: 800%; margin-left: 10%; margin-top: 10%;">TODAY'S <br>FACT! </h1>                                
    </div>
    <div class="col-md-8">
      <h4 class="text-center" style="margin-left: 140px; margin-top: 180px;">"Fashion with woven fabrics is also used in traditional dances in Sumba. generally used during religious ceremonies, harvesting, and even to welcome guests of honor from outside the region."
      </h4>                
    </div>
  </div>          
</div>  
</section>


<!-- OUR TOP PRODUCTS -->
<section class="best">
  <div class="col-md-12 col-mt-5">  
    <div class="container" style="margin-bottom: -10px;">
      <div class="row justify-content-center">
        <h1 class="text-center m-5 p-5"><b>OUR BEST PRODUCTS</b></h1>
        <div class="col-md-3">
          <div class="card" style="width: 19rem; height: 40rem; margin-top: 100px;">
            <img src="img/best1.jpg" class="card-img-top">
            <div class="card-body">
              <p class="card-text">International Cultural Festival 2019 / Lau Pahikung.</p>
            </div>
          </div>
        </div>

        <div class="col-md-3">
          <div class="card">
            <img src="img/best2.jpg" class="card-img-top">
            <div class="card-body">
              <p class="card-text" style=" margin-left: 3px;">Luna Maya at Prai'ijing Village wearing Lau Pahudu Kiku</p>
            </div>
          </div>
        </div>

        <div class="col-md-3">
          <div class="card" style="width: 18rem; height: 40rem; margin-top: 100px;">
            <img src="img/best3.jpg" class="card-img-top">
            <div class="card-body">
              <p class="card-text">Hongkong Fashion Show 2019 / Lau Pahikung.</p>
            </div>
          </div>
        </div>
      </div>
    </div>          
  </div>

</section>


<!-- Sponsored By  -->
<section  class="team">
 <div class="col-md-12">
   <h1 style="margin-left: 5%;"><b>Our team & Sponsors</b></h1>
   <div class="row justify-content-center">
    <div class="col-md-6 mt-5">
      <img src="img/LOGO.jpg" style="width:80%; height: 100%; margin-left: 15%">  
    </div>

    <div class="col-md-6 mt-5">
      <img src="img/galeri.jpg" style="width:80%; height: 80%; margin-left: 5%;">  
    </div>

    <p class="text-center"><i>All of this was tributed to Sumba. <br>Someday, there will be a movement where young people of Sumba will raise and stand for their region. The cultural wealth will be known around the world, and Sumba will have their lights shinning in a bright future ahead.</i>
    </p>
  </div>

</div>
</section>


</body>

<footer class="bg-light text-center text-lg-start">  
  <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
    © 2021 Copyright:
    <a class="text-dark" href="index.php">KAINTENUNKU.com</a>
  </div>
</footer>


<!-- Optional JavaScript; choose one of the two! -->

<!-- Option 1: Bootstrap Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>

<script src="function.js"></script>

<!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js" integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js" integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG" crossorigin="anonymous"></script>
  -->
  
  </html>