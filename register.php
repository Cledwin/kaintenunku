<?php 
include 'connection.php';
include 'function.php';
?>

<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="styleLogin.css">
  <link rel="preconnect" href="https://fonts.gstatic.com">    
  <link href="https://fonts.googleapis.com/css2?family=Playfair+Display&family=Work+Sans&display=swap" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="css/styleLogin.css">
  <title>KainTenunKu - Register</title>
</head>

<body>

  <section class="Form my-4 mx-5">
    <div class="container">
      <div class="row no-gutters" id="row">

        <div class="col-lg-5">
          <img src="./img/regtenun.jpg" class="img-fluid">
        </div>

        <div class="col-lg-7 px-5 pt-3">
          <h1 class="font-weight-bold">Register</h1>
          <label> Register as a new user in KainTenunKu!</label>

          <form method="POST" action="#">
            <div class="form-row my-2">
              <div class="col-lg-7">                  
                <input type="text" class="form-control" placeholder="Name" required  name="name" autocomplete="off">
              </div>
            </div>

            <div class="form-row my-4">
              <div class="col-lg-7">
                <input type="email" class="form-control" placeholder="Email" required name="email" autocomplete="off">
              </div>
            </div>              

            <div class="form-row my-4">
              <div class="col-lg-7">
                <input type="password" class="form-control" placeholder="Password"required name="password" autocomplete="off">
              </div>
            </div>

            <div class="form-row my-4">
              <div class="col-lg-7">
                <input type="text" class="form-control" placeholder="Address" required name="address" autocomplete="off">
              </div>
            </div> 

            <div class="form-row my-4">
              <div class="col-lg-7">
                <input type="number" class="form-control" placeholder="Phone Number" required name="phone" autocomplete="off">
              </div>
            </div>             

            <div class="form-row col-md-7 my-2">
              <div class="input-group mb-3">
                <select name="level"class="form-select" id="inputGroupSelect04" aria-label="Example select with button addon">
                  <label>Choose...</label>
                  <option value="Customer">Customer</option>
                  <option value="Seller">Seller</option>                  
                </select>
              </div>
            </div>

            <div class="row justify-content-start">
              <div class="col-lg-4">
                <button name="submit" type="submit" class="btn2 mt-3 mb-4"> Register </button>
              </div>                    
            </div>

            <?php 
            if (isset($_POST['submit'])) {

              $name = $_POST['name'];
              $address = $_POST['address'];
              $phone = $_POST['phone'];
              $email = $_POST['email'];
              $password = $_POST['password'];
              $level = $_POST['level'];

              $sql = "INSERT INTO data_user VALUES('','$name','$email','$password','$address','$phone','$level')";

              $query = mysqli_query($connect, $sql);      

              if($query){
                displayAlert("success","Successfully Register");
                header("refresh:2;url=login.php");
              }

              else{
                displayAlert("danger","Failed Register User!");
                header("refresh:2;url=login.php"); 
              }

            }            
            
            ?>               
          </form>            
        </div>
      </div>
    </div>
  </section>





















  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-p34f1UUtsS3wqzfto5wAAmdvj+osOnFyQFpp4Ua3gs/ZVWx6oOypYoCJhGGScy+8" crossorigin="anonymous"></script>

</body>
</html>