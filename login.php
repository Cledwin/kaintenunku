<?php 
  include 'connection.php';  
  include 'function.php';

  error_reporting(E_ALL ^ E_NOTICE);
  session_start();
  ?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="styleLogin.css">
  <link rel="preconnect" href="https://fonts.gstatic.com">    
  <link href="https://fonts.googleapis.com/css2?family=Playfair+Display&family=Work+Sans&display=swap" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="css/styleLogin.css">

  <title>KainTenunKu - User Login</title>
</head>

<body>

  <section class="Form my-4 mx-5">
    <div class="container">
      <div class="row no-gutters" id="row">

        <div class="col-lg-5">
          <img src="./img/tenun.png" class="img-fluid">
        </div>

        <div class="col-lg-7 px-5 pt-5">
          <h1 class="font-weight-bold py-3">KainTenunKu - Login </h1>

          <form method="POST" action="#">
            <div class="form-row my-3">
              <div class="col-lg-7">
                <input name="name" type="text" class="form-control" placeholder="Username" class="form-control" autocomplete="off" required>
              </div>
            </div>

            <div class="form-row my-4">
              <div class="col-lg-7">
                <input name="password" type="password" class="form-control" placeholder="Password" class="form-control" required>
              </div>
            </div>

            <div class="form-row my-4">
              <div class="col-lg-7">
                <button name="login" type="submit" class="btn1 nt-3 mb-3"> Login </button>
              </div>
            </div>

            <?php 
              if(isset($_POST['login'])){
                $name = $_POST['name'];
                $pwd = $_POST['password'];

                $sql = "SELECT * FROM data_user WHERE name='$name' AND password='$pwd'";
                $query = mysqli_query($connect,$sql);
                $row = mysqli_num_rows($query);

                //if row in database is not null
                if($row!=0){ 
                  $data = mysqli_fetch_assoc($query);                  
            
                  if($data['level'] == "Customer"){
                      $_SESSION['name'] = $name;
                      $_SESSION['level']= "Customer";
                      //redirect to Customer page
                      displayAlert("success", "Login Success! Redirecting to your page!");
                      header("refresh:1;url=customerIndex.php"); 
                  }

                  else if($data['level'] == "Seller"){
                      $_SESSION['name'] = $name;
                      $_SESSION['level']= "Seller";
                      //redirect to Seller page
                      displayAlert("success", "Login Success! Redirecting to your page!");
                      header("refresh:1;url=sellerHome.php"); 
                  } 
                  
                  else if($data['level'] == "admin"){
                      $_SESSION['name'] = $name;
                      $_SESSION['level']= "admin";
                      //redirect to admin page
                      displayAlert("success", "Login Success! Redirecting to your page!");
                      header("refresh:1;url=adminpage.php"); 
                  }                   

                  else{
                    displayAlert("danger","Incorrect Username/Password! Please enter the correct one!");
                    header("refresh:10;url=login.php");   
                  }
                }

                else{
                  displayAlert("danger","Login failed! Try again!");
                  header("refresh:10;url=login.php");      
                }
              }
            ?>
          </form>

          <p>Don't have account? <a id="reg" href="register.php">Register Now!</a></p>        
        </div>        
      </div>      
    </div>
  </section>



  


















    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-p34f1UUtsS3wqzfto5wAAmdvj+osOnFyQFpp4Ua3gs/ZVWx6oOypYoCJhGGScy+8" crossorigin="anonymous"></script>

    </body>
    </html>