<?php 
include 'connection.php';
include 'function.php';

  error_reporting(E_ALL ^ E_NOTICE);
  session_start();

if ($_SESSION['name'] == "" && $_SESSION['level'] == "") {
  displayAlert("alert","You need to login first!");
  header("Location: login.php");
}
?>

<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Bootstrap CSS -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <!-- font style -->
  <link rel="preconnect" href="https://fonts.gstatic.com">  
  <!-- <link href="https://fonts.googleapis.com/css2?family=DM+Sans&display=swap" rel="stylesheet"> -->
  <!--   <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200&display=swap" rel="stylesheet">   -->
  <link href="https://fonts.googleapis.com/css2?family=Playfair+Display&family=Work+Sans&display=swap" rel="stylesheet">  
  <script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>

  <title>KainTenunKu-Seller</title>

</head>
<body>
  <!-- NAVBAR --> 
  <header>        
    <nav class="navbar navbar-expand-lg navbar-light bg-white fixed-top">
      <div class="container-fluid">
        <a class="navbar-brand" href="#">
          <img class="logo" src="img/logokecil.png" width="110" height="50">
        </a>        
        <div class="collapse navbar-collapse col-md-8" id="navbarNav">          
          <ul class="navbar-nav navbar-right">
            <li class="nav-item">
              <a class="nav-link " aria-current="page" href="sellerHome.php">Home</a>
            </li>

            <li class="nav-item">
              <a class="nav-link " aria-current="page" href="sellerProduct.php">Product List</a>
            </li>

            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Manage Product
              </a>
              <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                <li><a class="dropdown-item" href="addProduct.php">Add Product</a></li>
                <li><a class="dropdown-item" href="#">Edit Product</a></li>
                <li><a class="dropdown-item" href="#">Delete Product</a></li>
              </ul>
            </li>

            <li class="nav-item">              
              <a class="nav-link " href="#">Payment</a>
            </li>            
            <li class="nav-item">
              <a class="nav-link " href="#">Chat</a>
            </li>
            <li class="nav-item">
              <a class="nav-link " href="logout.php">Logout</a>
            </li>                                                                            
          </ul>        
        </div>
        <div class="navbar navbar-nav col-md-1 col-xs-1">                        
          <div class="collapse navbar-collapse">
            <?php echo $_SESSION['name'].''."<b><p class='card-text'><i class='fas fa-user-alt' style='margin-left:10px;font-size:23px'></i></p></b>" ?>
          </div>          
        </div> 
      </div> 

      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>                
    </nav>    
  </header>  

  <div class="container" style="margin-top: 7%;">  
    <div class="card">
      <div class="card-header text-center bg-warning">
        <h5 class="mt-2">Add New Product</h5>
      </div>

      <div class="card-body">
        <form method="post" action="addProduct.php" enctype="multipart/form-data">
          <div class="row justify-content-start">
            <div class="col-6">
              <div class="mb-3">
                <label for="name" class="form-label">Product Name</label>
                <input type="text" name="name" class="form-control" id="name" autocomplete="off">
              </div>
            </div>

            <div class="col-4">
              <div class="mb-3">
                <label for="type" class="form-label">Product Type</label>
                <select name="type" class="form-select" id="inputGroupSelect01">
                  <option selected>Anahida</option>
                  <option value="Hinggi Kombu">Hinggi Kombu</option>
                  <option value="Kaleku">Kaleku</option>
                  <option value="Tabelo">Tabelo</option>
                  <option value="Mamuli">Mamuli</option>
                  <option value="Haikara">Haikara</option>
                </select>
              </div>
            </div>
          </div>

          <div class=" col-10 mb-3">
            <label for="number" class="form-label">Price</label>
            <input type="number" name="price" class="form-control" id="price" placeholder="$">
          </div>

          <div class="col-10 mb-3">
            <label for="quantity" class="form-label">Quantity</label>
            <input type="number" name="quantity" class="form-control" id="quantity">
          </div>

          <div class="mb-3">
            <label for="textarea" class="form-label">Product Description</label>
            <textarea class="form-control" name="description" id="textarea" rows="3"></textarea>
          </div>

          <div class="mb-3">
            <label for="formFile" class="form-label">Images</label>
            <input class="form-control" name="images" type="file" id="formFile">
          </div>

          <div class="col-4">
            <button name="submit" type="submit" class="btn btn-success mb-3">Add Product</button>
          </div>

          <?php 
          if (isset($_POST['submit'])) {

            $name = $_POST['name'];            
            $type = $_POST['type'];
            $price = $_POST['price'];
            $quantity = $_POST['quantity'];
            $description = $_POST['description'];
            $images = $_FILES['images']['name'];    
            $temp = $_FILES['images']["tmp_name"];
            $folder = './img/upload/';

            move_uploaded_file($temp, $folder.$images);

            $sql = "INSERT INTO product 
            VALUES('','$name','$type','$price','$quantity','$description','$images')";

            $query = mysqli_query($connect, $sql);      

            if($query){
              displayAlert("success","New Product Added!");              
            }

            else{
              displayAlert("danger","Failed Add Product!");
              header("refresh:2;url=addProduct.php"); 
            }

          }            

          ?> 
        </form>   
      </div>
    </div>
  </div>

  <footer class="bg-light text-center text-lg-start">
    <!-- Copyright -->
    <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
      © 2021 Copyright:
      <a class="text-dark" href="index.php">KAINTENUNKU.com</a>
    </div>
    <!-- Copyright -->
  </footer>

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>

  <script src="function.js"></script>

  <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js" integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js" integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG" crossorigin="anonymous"></script>
  -->
  
  </html>