<?php 
session_start();

function displayAlert($type,$text){
	echo "<div class='alert alert-".$type." mt-2' role='alert'>
	".$text."</div>";    
}

function displayProduct($id){    
	$connect = mysqli_connect("localhost","root","","kaintenunku");		
	$sql = "SELECT * FROM product WHERE type = '$id'";	
	$data = mysqli_query($connect,$sql);	
      while($display = mysqli_fetch_array($data))
      {                         
        echo "<div class='col-lg-4 mt-2 mb-2'>";
        echo "<div class='card' style='border-radius:5px; box-shadow: 4px 4px lightgrey;'>";
        echo "<img src='img/upload/".$display['product_img']." 
        'style= 
        'width: 95%; height:20rem; border-radius:10px;''>";
        echo "<div class='card-body'>";
        echo '<h5 class="card-title">'.$display['productName'].'</h5>';        
        echo '<h5 class="card-title">$'.$display['price'].' </h5>';
        echo '<p class="card-text">'.$display['description'].'</p>';

        for ($i=0; $i < 5; $i++) { 
           echo "<i class=' justify-content-center fas fa-star'></i>";
       }        
       echo "</div>";
       echo "</div>";
       if ($_SESSION['level'] == "Seller") { 
        echo "<div class='row justify-content-center'>";
        echo "<div class='col-4'>";           
        echo "<a href='editProduct.php' class='btn btn-success'>Edit <i class='far fa-edit'></i></a>";
        echo "</div>";

        echo "<div class='col-4'>";           
        echo "<a href='deleteProduct.php?id=".$display['productID']."'class='btn btn-danger'>Delete<i class='far fa-trash-alt'></i></a>";            

        echo "</div>";
        echo "</div>";            
    }

    elseif ($_SESSION['level'] == "Customer") {
        $id = $display['productID'];                
        echo "<form action='addtocart.php' method='post'>";
        echo "<input type='hidden' name='id' value=$id>";
        echo "<button class='btn btn-warning'type='submit' name='add_to_cart'>Add to Cart<i class='fas fa-shopping-cart'></i></button>";
        echo "</form>";
    }

    echo "</div> ";
    }
}

function displayBadge(){
    $user = $_SESSION['id'];
    $badge = 0;
    $connect = mysqli_connect("localhost","root","","kaintenunku");
    $sql = "SELECT * FROM cart WHERE user_id = $user";
    $row = mysqli_query($connect,$sql);

    while ($display = mysqli_fetch_array($row)){
        $badge++;
    }
    echo $badge;
}

function displayCart(){            
    $user = $_SESSION['id'];
    $no = 1;         
    $connect = mysqli_connect("localhost","root","","kaintenunku");             
    $sql = "SELECT * FROM cart JOIN product on product_id = productID WHERE user_id= $user AND status = 0";
    $data = mysqli_query($connect,$sql);
    $total = 0;    
    while($display = mysqli_fetch_array($data)){                    
        echo "<tr>";        
        echo "<td scope='row'>";
        echo "<p class='m-2'></p>".$no++.".";
        echo "</td>"; 
        echo "<td scope='row'>";
        echo "<img src='img/upload/".$display['product_img']." 'style= 'width: 60px; height:60px;''>";
        echo "</td>";

        echo "<td scope='row'>";
        echo "<p class='m-2'></p>".$display['productName'];
        echo "</td>";            

        echo "<td scope='row'>";
        echo '<p class="card-text m-2">'.$display['price'].'</p>';
        echo "</td>";

        echo "<td scope='row'>";
        echo '<p class="card-text m-2">'.$display['quantity'].'</p>';
        echo "</td>";        

        echo "</div>";
        echo "</td>";

        echo "<td scope='row'>";
        echo '<p class="card-text m-2">$'.number_format($display['price']*$display['quantity']).'</p>';        
        echo "</td>";

        echo "<td scope='row'>
                <a href='deleteCart.php?id=".$display['id']."'class='btn btn-danger m-2'><i class='far fa-trash-alt'></i></a>
                </td>";
        echo "</tr>";


        $total+= $display['price'] * $display['quantity'];                        
    }    
    return $total;
}

?>